import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { Router, browserHistory } from 'react-router'
import style from 'styles/main.scss'
import store from './store'
import routes from './routes'
import injectTapEventPlugin from 'react-tap-event-plugin'

injectTapEventPlugin()

render(
  <Provider store={store}>
    <Router routes={routes(store)} history={browserHistory} />
  </Provider>,
  document.getElementById('niser-app')
)
