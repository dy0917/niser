import keyMirror from 'key-mirror'

export default keyMirror({
  SIGNUP_REQUEST: null,
  SIGNUP_SUCCESS: null,
  SIGNUP_FAILURE: null,

  LOGIN_REQUEST: null,
  LOGIN_SUCCESS: null,
  LOGIN_FAILURE: null,

  LOGOUT_REQUEST: null,
  LOGOUT_SUCCESS: null,
  LOGOUT_FAILURE: null,

  FETCH_EVENTS_REQUEST: null,
  FETCH_EVENTS_SUCCESS: null,
  FETCH_EVENTS_FAILURE: null
})
