import React, { createClass, PropTypes } from 'react'
import LeftNav from 'material-ui/Drawer'
import { List, ListItem, MakeSelectable } from 'material-ui/List'
import Divider from 'material-ui/Divider'
import { colors, spacing, typography } from 'material-ui/styles'

const SelectableList = MakeSelectable(List)

const AppLeftNav = createClass({
  propTypes: {
    docked: PropTypes.bool.isRequired,
    history: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    onRequestChangeLeftNav: PropTypes.func.isRequired,
    onRequestChangeList: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    style: PropTypes.object
  },

  contextTypes: {
    muiTheme: PropTypes.object
  },

  handleRequestChangeLink(event, value) {
    window.location = value
  },

  handleTouchTapHeader() {
    this.props.history.push('/')
    this.setState({
      leftNavOpen: false
    })
  },

  getStyles() {
    return {
      logo: {
        cursor: 'pointer',
        fontSize: 24,
        color: typography.textFullWhite,
        lineHeight: `${spacing.desktopKeylineIncrement}px`,
        fontWeight: typography.fontWeightLight,
        backgroundColor: colors.cyan500,
        paddingLeft: spacing.desktopGutter,
        marginBottom: 8
      }
    }
  },

  render() {
    const {location, docked, onRequestChangeLeftNav, onRequestChangeList, open, style} = this.props
    const { prepareStyles } = this.context.muiTheme
    const styles = this.getStyles()

    return (
    <LeftNav
      style={style}
      docked={docked}
      open={open}
      onRequestChange={onRequestChangeLeftNav}>
      <div style={prepareStyles(styles.logo)} onTouchTap={this.handleTouchTapHeader}>
        Niser
      </div>
      <SelectableList>
        <ListItem primaryText="Events" value="url" />
        <ListItem primaryText="Planning" value="url" />
        <ListItem primaryText="Organization" value="url" />
        <ListItem primaryText="Members" value="url" />
        <ListItem primaryText="Settings" value="url" />
        <ListItem primaryText="Signin" value="/signin" />
      </SelectableList>
    </LeftNav>
    )
  }
})

export default AppLeftNav
