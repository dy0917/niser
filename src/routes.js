import React from 'react'
import { Route, IndexRoute } from 'react-router'
import * as pages from './pages'

const {EventsPage, SignupPage, LoginPage, Master, Playground} = pages

export default (store) => (
  <Route path="/">
    <Route path='signup' component={SignupPage} />
    <Route path='login' component={LoginPage} />
    <Route path='admin' component={Master}>
      <IndexRoute component={EventsPage} />
    </Route>
    <Route path='playground' component={Playground} />
  </Route>
)
