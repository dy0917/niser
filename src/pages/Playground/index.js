import React from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'

import RaisedButton from 'material-ui/RaisedButton'

import CSSModules from 'react-css-modules'
import moduleStyles from './style.scss'

const Playground = React.createClass({
  getInitialState() {
    return {}
  },

  componentWillMount: function () {
    const {dispatch} = this.props
  },

  componentWillUnmount: function () {},

  buttonFunction: function () {},

  render() {
    return (
    <div className="playground">
      <RaisedButton label="Add event" onClick={this.buttonFunction} />
    </div>
    )
  }
})

const mapStateToProps = (appState) => ({
  state: {}
})

const CSSModulesPlayground = CSSModules(Playground, moduleStyles)

export default connect(mapStateToProps)(CSSModulesPlayground)
