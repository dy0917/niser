import React from 'react'
import AppBar from 'material-ui/AppBar'
import * as components from '../components'
import { colors, spacing } from 'material-ui/styles'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
const { AppLeftNav } = components

const Wrapper = React.createClass({
  propTypes: {
    children: React.PropTypes.node,
    history: React.PropTypes.object,
    location: React.PropTypes.object
  },

  childContextTypes: {
    muiTheme: React.PropTypes.object
  },

  getInitialState() {
    return {
      muiTheme: getMuiTheme(),
      navDrawerOpen: true
    }
  },

  getChildContext() {
    return {
      muiTheme: this.state.muiTheme
    }
  },

  componentWillMount() {
    this.setState({
      muiTheme: this.state.muiTheme
    })
  },

  getStyles() {
    const styles = {
      appBar: {
        position: 'fixed',
        // Needed to overlap the examples
        zIndex: this.state.muiTheme.zIndex.appBar + 1,
        top: 0
      },
      root: {
        paddingTop: spacing.desktopKeylineIncrement,
        minHeight: 400,
        paddingLeft: 256
      },
      content: {
        margin: spacing.desktopGutter
      }
    }

    return styles
  },

  handleChangeRequestLeftNav(open) {
    this.setState({
      leftNavOpen: open
    })
  },

  handleRequestChangeList(event, value) {
    this.props.history.push(value)
    this.setState({
      navDrawerOpen: false
    })
  },

  handleChangeMuiTheme(muiTheme) {
    this.setState({
      muiTheme: muiTheme
    })
  },

  render() {
    const {history, location, children} = this.props
    const styles = this.getStyles()
    const { prepareStyles } = this.state.muiTheme

    let {navDrawerOpen} = this.state
    let docked = true

    styles.leftNav = {
      zIndex: styles.appBar.zIndex - 1
    }

    return (
    <div>
      <AppBar style={styles.appBar} />
      <div style={prepareStyles(styles.root)}>
        <div style={prepareStyles(styles.content)}>
          {children}
        </div>
      </div>
      <AppLeftNav
        style={styles.leftNav}
        history={history}
        location={location}
        docked={docked}
        onRequestChangeLeftNav={this.handleChangeRequestLeftNav}
        onRequestChangeList={this.handleRequestChangeList}
        open={navDrawerOpen} />
    </div>
    )
  }
})

export default Wrapper
