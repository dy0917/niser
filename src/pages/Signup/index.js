import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'

import Paper from 'material-ui/Paper'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import getMuiTheme from 'material-ui/styles/getMuiTheme'

import * as authActions from '../../actions/Auth'
import CSSModules from 'react-css-modules'
import moduleStyles from './style.scss'

const {signupWithEmail} = authActions

export const fields = ['username', 'password']

class SignupPage extends Component {
  constructor (props, context) {
    super(props, context)
    this.state = {
      muiTheme: getMuiTheme()
    }
    this.handleSignup = this.handleSignup.bind(this)
  }

  getChildContext () {
    return {
      muiTheme: this.state.muiTheme
    }
  }

  componentWillMount () {
    this.setState({
      muiTheme: this.state.muiTheme
    })
  }

  handleSignup (event) {
    event.preventDefault()

    const email = this.refs.email.input
    const password = this.refs.password.input

    this.props.dispatch(signupWithEmail(email.value, password.value))

    email.value = ''
    password.value = ''
  }

  render () {
    const { prepareStyles } = this.state.muiTheme

    const {fields: {username, password}, auth, authError} = this.props

    const firebaseError = function (authError) {
      let error = ''

      if (authError && (authError.code === 'EMAIL_TAKEN')) {
        error = authError.message
      }

      return error
    }

    const styles = {
      textField: {
        width: 360,
        zIndex: 9001,
        marginTop: 0
      },
      inputStyle: {
        zIndex: 1
      },
      button: {
        width: 360,
        fontSize: 18,
        height: '2.5em',
        lineHeight: '2.5em',
        marginTop: 20,
        marginBottom: 20
      },
      buttonLabel: {
        fontSize: 16
      },
      altAction: {
        textAlign: 'center',
        marginTop: '10px',
        lineHeight: '4em'
      },
      loginText: {
        color: '#FFFFFF'
      },
      loginLink: {
        color: '#FFFFFF',
        cursor: 'pointer'
      }
    }

    return (
    <div styleName="page">
      <Paper style={styles.paper} zDepth={5}>
        <form styleName="form">
          <div styleName="form-inner">
            <div styleName="logo">
              <div styleName="logo-img" />
            </div>
            <TextField
              ref="email"
              style={styles.textField}
              inputStyle={styles.inputStyle}
              hintText="Email address"
              floatingLabelText="Email address"
              errorText={username.touched && username.error && username.error}
              {...username} />
            <TextField
              ref="password"
              style={styles.textField}
              inputStyle={styles.inputStyle}
              hintText="password"
              floatingLabelText="password"
              type="password"
              errorText={password.touched && password.error && password.error}
              {...password} />
            <div styleName="error-message">
              {firebaseError(authError)}
            </div>
            <RaisedButton
              styleName="button"
              style={styles.button}
              labelStyle={styles.buttonLabel}
              label="Sign up"
              onClick={this.handleSignup}
              primary={true} />
          </div>
        </form>
      </Paper>
      <div style={styles.altAction}>
        <span style={styles.loginText}>Already have an account?</span>
        <Link style={styles.loginLink} to="/login"> Login now.
        </Link>
      </div>
    </div>
    )
  }
}

SignupPage.propTypes = {
  muiTheme: PropTypes.object,
  fields: PropTypes.object.isRequired,
  auth: PropTypes.object,
  expires: PropTypes.node,
  password: PropTypes.object,
  token: PropTypes.string,
  loading: PropTypes.bool,
  authError: PropTypes.object,
  dispatch: PropTypes.func.isRequired
}

SignupPage.childContextTypes = {
  muiTheme: PropTypes.object
}

const validate = (values) => {
  const errors = {}

  if (!values.username) {
    errors.username = 'Required'
  } else if (values.username.length > 15) {
    errors.username = 'Must be 15 characters or less'
  }

  if (!values.password) {
    errors.password = 'Required'
  }

  return errors
}

function mapStateToProps ($$state) {
  return $$state.get('auth').toJS()
}

const CSSModulesSignupPage = CSSModules(SignupPage, moduleStyles)

const reduxFormSignupPage = reduxForm({
  form: 'signup',
  fields: fields,
  getFormState: (state, reduxMountPoint) => state.get(reduxMountPoint).toJS(),
  validate: validate
})(CSSModulesSignupPage)

export default connect(mapStateToProps)(reduxFormSignupPage)
