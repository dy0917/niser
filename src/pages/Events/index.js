import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'

import RaisedButton from 'material-ui/RaisedButton'
import getMuiTheme from 'material-ui/styles/getMuiTheme'

import * as eventsActions from '../../actions/Events'
import CSSModules from 'react-css-modules'
import moduleStyles from './style.scss'

const { getEvents } = eventsActions

class EventsPage extends Component {
  constructor (props, context) {
    super(props, context)
    this.state = {
      muiTheme: getMuiTheme()
    }
    this.buttonFunction = this.buttonFunction.bind(this)
  }

  getChildContext () {
    return {
      muiTheme: this.state.muiTheme
    }
  }

  componentWillMount () {
    this.setState({
      muiTheme: this.state.muiTheme
    })
  }

  buttonFunction () {
    this.props.dispatch(getEvents())
  }

  render () {
    // const resultList = this.props.state.events.map((result) => (
    //   <li key={result.key}>
    //     {result.id}
    //   </li>
    // ))

    return (
    <div className="eventlist">
      <RaisedButton label="Add event" onClick={this.buttonFunction} />
      <ol></ol>
    </div>
    )
  }
}

EventsPage.propTypes = {
  muiTheme: PropTypes.object,
  dispatch: PropTypes.func.isRequired
}

EventsPage.childContextTypes = {
  muiTheme: PropTypes.object
}

function mapStateToProps ($$state) {
  return $$state.get('events').toJS()
}

const CSSModulesEventsPage = CSSModules(EventsPage, moduleStyles)

export default connect(mapStateToProps)(CSSModulesEventsPage)
