import { BASE, FirebaseService } from './index'

const ref = FirebaseService.rootRef()
const groupsRef = ref.child('groups')

const GroupService = {
  newGroup: function (name, creatorUid) {
    groupsRef.push().set({
      name: name,
      creator: creatorUid,
      createdAt: now()
    })
  },

  getPublicGroup: function () {
    return new Promise(function (resolve, reject) {
      groupsRef.child('public').on('value', function (snapshot) {
        if (snapshot.val()) {
          resolve(snapshot.val())
        } else {
          const publicGroup = {
            name: 'public'
          }

          groupsRef.child('public').set(publicGroup, function () {
            resolve(publicGroup)
          })
        }
      })
    })
  }
}

export default GroupService
