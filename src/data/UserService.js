import path from 'path'
import firebase from 'firebase'
import { FirebaseService } from './index'

const ref = FirebaseService.rootRef()
const usersRef = ref.child('users')

function getName (authData) {
  switch (authData.providerData[0].providerId) {
    case 'password':
      return authData.email.replace(/@.*/, '')
    case 'google':
      return authData.google.displayName
  }
}

const UserService = {
  isUserArchived: function (uid) {
    const archivedUserRef = FirebaseService.rootRef(path.join('users' , uid))

    return new Promise(function (resolve, reject) {
      archivedUserRef.on('value', function (snapshot) {
        resolve(!!snapshot.val())
      }, function (errorObject) {
        reject(errorObject.code)
      })
    })
  },

  archiveUser: function (onUserArchived) {
    firebase.auth().onAuthStateChanged(function(authData) {
      console.log('b:')
      console.log(authData)
      if (authData) {
        const userRef = ref.child('users').child(authData.uid)

        userRef.set({
          uid: authData.uid,
          providers: authData.providerData,
          name: getName(authData)
        }, onUserArchived)

        userRef.child('groups').child('public').set(true)
      }
    })
  }
}

export default UserService
