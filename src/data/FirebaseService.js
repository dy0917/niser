import firebase from 'firebase'
import config from 'config'

const firebaseConfig = {
  apiKey: config.apiKey,
  authDomain: config.authDomain,
  databaseURL: config.databaseURL,
  storageBucket: config.storageBucket
}

firebase.initializeApp(firebaseConfig)

const FirebaseService = {
  rootRef: (resource) => {
    return firebase.database().ref(resource)
  }
}

export default FirebaseService
