import { FirebaseService } from './index'

const ref = FirebaseService.rootRef()

const eventService = {
  createEventInGroup: function (groupId, event) {
    const eventsRef = ref.child('groups').child(groupId).child('events')

    eventsRef.push().set(event)
  }
}

export default eventService
