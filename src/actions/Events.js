import ActionTypes from '../constants/ActionTypes'

const {FETCH_EVENTS_REQUEST, FETCH_EVENTS_SUCCESS, FETCH_EVENTS_FAILURE} = ActionTypes

function fetchEventsRequest () {
  return {
    type: FETCH_EVENTS_REQUEST
  }
}

function fetchEventsSuccess (data) {
  return {
    type: FETCH_EVENTS_SUCCESS,
    events: data
  }
}

function fetchEventsFailure (error) {
  return {
    type: FETCH_EVENTS_FAILURE,
    error: error
  }
}

export function getEvents () {
  return (dispatch) => {
    dispatch(fetchEventsSuccess([
      {
        id: 1
      }, {
        id: 2
      }
    ]))
  }
}
