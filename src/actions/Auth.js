import firebase from 'firebase'
import ActionTypes from '../constants/ActionTypes'
import { browserHistory } from 'react-router'
import { AuthService, UserService } from 'dataService'
const {SIGNUP_REQUEST, SIGNUP_SUCCESS, SIGNUP_FAILURE, LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT_REQUEST, LOGOUT_SUCCESS, LOGOUT_FAILURE} = ActionTypes

function signupRequest (username) {
  return {
    type: SIGNUP_REQUEST,
    username: username
  }
}

function signupSuccess (data) {
  return {
    type: SIGNUP_SUCCESS,
    uid: data.uid
  }
}

function signupFailure (authError) {
  return {
    type: SIGNUP_FAILURE,
    authError: authError && authError
  }
}

function loginRequest (username) {
  return {
    type: LOGIN_REQUEST,
    username: username
  }
}

function loginSuccess (data) {
  return {
    type: LOGIN_SUCCESS,
    auth: data.auth,
    expires: data.expires,
    password: data.password,
    token: data.token
  }
}

function loginFailure (authError) {
  return {
    type: LOGIN_FAILURE,
    authError: authError && authError
  }
}

function logoutRequest (user) {
  return {
    type: LOGOUT_REQUEST,
    user: user
  }
}

function logoutSuccess (user, payload) {
  return {
    type: LOGOUT_SUCCESS,
    user: user,
    payload: payload
  }
}

function logoutFailure (user, authError) {
  return {
    type: LOGOUT_FAILURE,
    user: user,
    authError: authError && authError
  }
}

export function signupWithEmail (email, password) {
  return (dispatch) => {
    firebase.auth().createUserWithEmailAndPassword(email, password).then(function (data) {
      dispatch(signupSuccess(data))
      browserHistory.push('/login')
    }).catch(function (error) {
      console.log(error)
      dispatch(signupFailure(error))
    })
  }
}

export function loginWithEmail (username, password) {
  return (dispatch) => {
    dispatch(loginRequest(username))

    firebase.auth().signInWithEmailAndPassword(username, password).then(function (authData) {
      return UserService.isUserArchived(authData.uid).then(function (isArchived) {
        function onLoginSuccess (authData) {
          dispatch(loginSuccess(authData))
          browserHistory.push('/admin')

          return new Promise((resolve) => resolve(authData))
        }

        return isArchived ? onLoginSuccess(authData) : UserService.archiveUser(() => onLoginSuccess(authData))
      })
    }).catch(function (error) {
      console.log(error)
      dispatch(loginFailure(error))
    })
  }
}

export function loginWithGoogle () {
  return (dispatch) => {
    // AuthService.authWithOAuthPopup('google').then(function (data) {}).catch(function (error) {
    //   console.log(error)
    // })
  }
}

export function logout (user) {
  return (dispatch) => {
    firebase.auth().signOut().then(function () {
      dispatch(logoutRequest(user))
    }, function (error) {
      console.log(error)
    })
  }
}
