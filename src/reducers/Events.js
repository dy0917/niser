import Immutable from 'immutable'
import ActionTypes from '../constants/ActionTypes'
import { eventsState } from '../store/initialstate'

const { FETCH_EVENTS_REQUEST, FETCH_EVENTS_SUCCESS, FETCH_EVENTS_FAILURE } = ActionTypes

export default (state = eventsState , action = {}) => {
  switch (action.type) {
    case FETCH_EVENTS_REQUEST:
      return eventsState.merge({
        loading: true
      })

    case FETCH_EVENTS_SUCCESS:
      return eventsState.merge({
        loading: false,
        events: action.events
      })

    case FETCH_EVENTS_FAILURE:
      return eventsState.merge({
        loading: false,
        error: action.error
      })

    default:
      return state
  }
}
