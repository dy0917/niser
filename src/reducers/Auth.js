import Immutable from 'immutable'
import ActionTypes from '../constants/ActionTypes'
import { authState } from '../store/initialstate'
import { browserHistory } from 'react-router'

const {SIGNUP_REQUEST, SIGNUP_SUCCESS, SIGNUP_FAILURE, LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT_REQUEST, LOGOUT_SUCCESS, LOGOUT_FAILURE} = ActionTypes

export default (state = authState , action = {}) => {
  switch (action.type) {
    case SIGNUP_REQUEST:
      return authState.merge({
        loading: true,
        username: action.username
      })

    case SIGNUP_FAILURE:
      return authState.merge({
        loading: false,
        authError: {
          code: action.authError.code,
          message: action.authError.message
        }
      })

    case SIGNUP_SUCCESS:
      return authState.merge({
        loading: false,
        auth: {
          uid: action.uid
        }
      })

    case LOGIN_REQUEST:
      return authState.merge({
        loading: true,
        username: action.username
      })

    case LOGIN_SUCCESS:
      return authState.merge({
        loading: false,
        auth: action.auth,
        expires: action.expires,
        password: action.password,
        token: action.token
      })

    case LOGIN_FAILURE:
      return authState.merge({
        loading: false,
        authError: {
          code: action.authError.code,
          message: action.authError.message
        }
      })

    default:
      return state
  }
}
