import { combineReducers } from 'redux-immutable'
import { reducer as formReducer } from 'redux-form'
import Immutable from 'immutable'

import auth from './Auth'
import events from './Events'

export default combineReducers({
  auth,
  events,
  form: (state = Immutable.fromJS({}) , action) => Immutable.fromJS(formReducer(state.toJS(), action))
})
