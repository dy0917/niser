import { createStore, compose, applyMiddleware } from 'redux'
import rootReducer from '../reducers'
import initialState from './initialstate'
import thunk from 'redux-thunk'

let logger = (store) => (next) => (action) => {
  let result = next(action)
  return result
}

let createStoreWithMiddleware = compose(
  applyMiddleware(thunk),
  window.devToolsExtension ? window.devToolsExtension() : (f) => f
)(createStore)

let store = createStoreWithMiddleware(rootReducer)

export default store
