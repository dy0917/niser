import Immutable from 'immutable'

const initialState = {
  auth: {
    auth: {
      provider: null,
      uid: null
    },
    expires: null,
    password: null,
    token: null,

    loading: false,
    authError: null
  },
  events: {}
}

const state = Immutable.fromJS(initialState)

export let authState = state.get('auth')
export let eventsState = state.get('events')
