var path = require('path'),
  webpack = require('webpack'),
  autoprefixer = require('autoprefixer'),
  precss = require('precss')

var Dashboard = require('webpack-dashboard');
var DashboardPlugin = require('webpack-dashboard/plugin');
var dashboard = new Dashboard();

module.exports = {
  entry: [
    './src/index.js'
  ],
  output: {
    path: path.join(__dirname, 'assets'),
    publicPath: '/assets/',
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
    alias: {
      'dataService': path.join(__dirname, 'src/data'),
      'styles': path.join(__dirname, 'assets/styles/'),
      'components': path.join(__dirname, 'src/components/'),
      'config': path.join(__dirname, 'src/config'),
      'constants': path.join(__dirname, 'src/constants/')
    }
  },
  devtool: 'source-map',
  module: {
    loaders: [{
      test: /\.(js|jsx)$/,
      exclude: /(node_modules|bower_components)/,
      loader: ['babel'],
      query: {
        presets: ['es2015', 'react'],
        cacheDirectory: false
      }
    }, {
      test: /\.(png|jpg)$/,
      loader: 'url-loader'
    }, {
      test: /\.scss$/,
      loaders: [
        'style?sourceMap',
        'css?modules&importLoaders=1&localIdentName=[name]__[local]__[hash:base64:5]',
        'resolve-url',
        'sass?sourceMap'
      ]
    }, {
      test: /\.css$/,
      loaders: [
        'style?sourceMap',
        'css?modules&importLoaders=1&localIdentName=[name]__[local]__[hash:base64:5]'
      ]
    }],
    preLoaders: [{
      test: /\.js$/,
      loaders: ['eslint'],
      include: [new RegExp(path.join(__dirname, 'src'))]
    }]
  },
  postcss: function () {
    return [autoprefixer, precss]
  },
  stats: {
    colors: true
  },
  eslint: {
    configFile: '.eslintrc'
  },
  sassLoader: {
    includePaths: [path.resolve(__dirname, './assets/styles')]
  },
  plugins: [
    new DashboardPlugin(dashboard.setData),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ]
}
